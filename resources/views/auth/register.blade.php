@extends('app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Cadastrar novo usuário</div>
    <div class="panel-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Aqui estão alguns error que ocorreram:<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['method' => 'POST', 'url'=>'auth/register', 'class'=>'form', 'role'=>'form']) !!}

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Nome') !!}
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('lastname', 'Sobrenome') !!}
                        {!! Form::text('lastname', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('password', 'Senha') !!}
                        {!! Form::password('password', ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Confirmar senha') !!}
                        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail') !!}
                        {!! Form::email('email', null, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('cpf', 'CPF') !!}
                        {!! Form::text('cpf', null,['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('gender', 'Sexo') !!}
                        {!! Form::select('gender', ['' => 'Selecione', 'male'=>'Masculino', 'female'=>'Female'], null, ['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('birthdate', 'Data de Nascimento') !!}
                        {!! Form::input('date', 'birthdate', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary pull-right">Cadastrar</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection
