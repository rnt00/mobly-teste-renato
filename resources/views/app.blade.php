<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ elixir('css/all.css') }}" rel="stylesheet">

	@yield('header_scripts')
</head>
<body>
    <div class="container layout">

        @include('partials.nav')

        @include('partials.categories')

        <div class="content col-xs-9">
            @include('flash::message')
            @yield('content')
        </div>

	</div>

	@yield('footer_scripts')

	<script src="{{ elixir('js/all.js') }}" type="text/javascript"></script>

</body>
</html>
