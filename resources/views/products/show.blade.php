@extends('app')

@section('content')

<div class="product">

    <div class="row">
        <h1 class="product-title col-xs-12">{{$product->name}}</h1>
    </div>

    <div class="row">
        @unless( $product->categories->isEmpty() )
            <div class="col-xs-12">
                <strong>Categorias: </strong>
                @for($i=0;$i<count($product->categories->toArray());$i++)
                    @if($i>0) | @endif
                    <strong><a href="{{route('categories.index', [$product->categories[$i]->slug])}}">{{$product->categories[$i]->name}}</a></strong>
                @endfor
            </div>
        @endunless
    </div>

    <hr/>

    <div class="row">
        <div class="col-xs-6">
            <img width="100%" src="{{$product->getFullImage()}}" alt=""/>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12 product-price">
                   R$ {{ $product->price }}
                </div>
                <div class="col-xs-12">
                    {!! Form::open(['method'=>'POST', 'route' => ['cart.store', $product->id]]) !!}
                    <div class="row">
                        <div class="form-group col-xs-3">
                            {!! Form::label('quantity', 'Quantidade:') !!}
                            {!! Form::input('number', 'quantity', 1, ['class' => 'form-control', 'min'=>1, 'max'=>30, 'pattern'=>'^[0-9]', 'title'=>'Somente números entre 1 e 30']) !!}
                        </div>
                        <div class="form-group add-cart-submit col-xs-6">
                            {!! Form::submit("Adicionar ao carrinho", ['class' => 'btn btn-primary form-control']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-xs-12">
                    <p>{{ $product->description }}</p>
                </div>
            </div>
        </div>
    </div>

    <hr/>

    <div class="row">
        <h3>Características do Produto</h3>

        <table class="table table-striped">
            <tbody>
                {{-- default attributes --}}
                <tr>
                    <td>Largura: </td>
                    <td>{{$product->width}} cm</td>
                </tr>
                <tr>
                    <td>Altura: </td>
                    <td>{{$product->height}} cm</td>
                </tr>
                <tr>
                    <td>Profundidade: </td>
                    <td>{{$product->depth}} cm</td>
                </tr>
                <tr>
                    <td>Peso: </td>
                    <td>{{$product->weight}} kg</td>
                </tr>

                {{-- custom attributes --}}
                @unless($product->attributes->isEmpty())
                    @foreach($product->attributes as $attribute)
                    <tr>
                        <td>{{$attribute->name}}</td>
                        <td>{{$attribute->pivot->value}}</td>
                    </tr>
                    @endforeach
                @endunless
            </tbody>
        </table>

    </div>
</div>

@stop