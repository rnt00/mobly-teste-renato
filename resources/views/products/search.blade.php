@extends('app')

@section('content')

   <h1>Resultados da busca para: {{$keyword}}</h1>
<hr/>
    @if( count($products) < 1 )
        <p>Nenhum produto encontrado...</p>
    @else
        <div class="row">
         @foreach( $products as $product )
            @include('partials.product_list_item')
         @endforeach
        </div>
        <div class="row">
           {!! $products->render() !!}
        </div>
    @endif
@stop