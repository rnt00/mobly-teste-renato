@extends('app')

@section('content')

   <h1>Todos os Produtos</h1>
<hr/>
    @if( $products->isEmpty() )
        <p>Nenhum produto encontrado...</p>
    @else
        <div class="row">
         @foreach( $products as $product )
            @include('partials.product_list_item')
         @endforeach
        </div>
        <div class="row">
           {!! $products->render() !!}
        </div>
    @endif
@stop