<!-- PROD. ITEM -->
<div class="col-xs-3">
    <div class="thumbnail">

        <!-- IMAGE CONTAINER-->
        <a href="{{route('products.show', [$product->slug])}}">
            <img width="160" height="160" src="{{$product->getThumbImage()}}" alt="{{$product->name}}" title="{{$product->name}}" />
        </a>
        <!--END IMAGE CONTAINER-->

        <!-- CAPTION -->
        <div class="caption">
            <h4>{{$product->name}}</h4>
            <div class="row-fluid">
                <p class="lead">R${{$product->price}}</p>
            </div>
        </div>
        <!--END CAPTION -->
    </div>
    <!-- END: THUMBNAIL -->
</div>
<!-- PROD. ITEM -->