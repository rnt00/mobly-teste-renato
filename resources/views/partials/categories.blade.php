<div class="col-md-3">
    <h3>Categorias</h3>
    @if($categories_menu->isEmpty())

    <p>Nenhuma categoria encontrada...</p>

    @else

    <div class="row">
    <ul class="nav bs-docs-sidenav col-xs-11">
        @foreach($categories_menu as $category)
        <li class="">
          <a href="{{route('categories.index', [$category->slug])}}">{{$category->name}}</a>
        </li>
        @endforeach
    </ul>

    </div>
    @endif
</div>