<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}">Mobly Teste</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    {!! Form::open(['method'=>'GET', 'route'=>'products.search',  'class'=>'navbar-form', 'role'=>'search']) !!}
                        {!! Form::label('q', 'Buscar:', ['class'=>'control-label']) !!}
                        <div class="input-group">
                            {!! Form::text('q', null, ['class'=>'form-control', 'placeholder'=>'Nome do Produto...']) !!}
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </li>
                <li>
                    <a href="{{ route('cart.index') }}">Carrinho de compras <strong>({{$cart_count}})</strong></a>
                </li>
                @if (Auth::guest())
                    <li><a href="{{ url('/auth/login') }}">Entrar</a></li>
                    <li><a href="{{ url('/auth/register') }}">Cadastrar</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

