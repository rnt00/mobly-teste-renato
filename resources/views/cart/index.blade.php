@extends('app')

@section('content')

<div class="row">
    <h1>Carrinho de compras</h1>
    <hr/>
</div>

@if($cart->isEmpty())
<div class="row">
    <p>O carrinho de compras está vazio.</p>
</div>
@else
    {!! Form::open(['method'=>'POST', 'route'=>'cart.update']) !!}
    <div class="row">
        <table class="table table-striped col-xs-12">
            <thead>
                <tr>
                    <th>Produto</th>
                    <th>Quantidade</th>
                    <th>Preço Unitário</th>
                    <th>Subtotal</th>
                    <th>Remover</th>
                </tr>
            </thead>

            <tbody>
            @foreach($cart as $row)
                <tr>
                    <td>
                        <p><strong><a href="{{route('products.show', [$row->options->slug])}}">{{ $row->name }}</a></strong></p>
                    </td>
                    <td><input type="number" name="qty[{{$row->rowid}}]" min="1" max="30" value="{{ $row->qty }}"></td>
                    <td>R$ {{ str_replace('.', ',', $row->price) }}</td>
                    <td>R$ {{ str_replace('.', ',', $row->subtotal) }}</td>
                    <td><a href="#" data-href="{{route('cart.remove', [$row->rowid])}}" data-toggle="modal" data-target="#confirm-delete-{{$row->id}}" class="btn btn-danger">Remover</a></td>
                    <div class="modal fade" id="confirm-delete-{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4>Deseja realmente remover este produto do carrinho?</h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <a href="{{route('cart.remove', [$row->rowid])}}" class="btn btn-danger btn-ok">Remover</a>
                                </div>
                            </div>
                        </div>
                    </div>
               </tr>

               @endforeach

               <tr>
                    <td>&nbsp;</td>
                    <th>{{$total_qty}}</th>
                    <td>&nbsp;</td>
                    <th>R$ {{ str_replace('.', ',', $total_price) }}</th>
                    <td></td>
               </tr>

            </tbody>
        </table>
    </div>
    <div class="row">
        <a href="#" data-href="{{route('cart.clear')}}" data-toggle="modal" data-target="#confirm-delete-all" class="btn btn-danger">Remover todos os produtos</a>
        <div class="modal fade" id="confirm-delete-all" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4>Deseja realmente remover todos os produtos do carrinho?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <a href="{{route('cart.clear')}}" class="btn btn-danger btn-ok">Remover</a>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::submit("Atualizar Carrinho", ['class' => 'btn btn-primary']) !!}
        <a class="btn btn-success" href="{{route('orders.index')}}">Finalizar pedido</a>
    </div>
    {!! Form::close() !!}
@endif

@stop