@extends('app')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <h3>Dados do Pedido - Número: {{$order->number}}</h3>
        <hr/>
    </div>
</div>

<div class="row">
<div class="col-xs-12">
<table class="table table-striped">
    <thead>
        <tr>
            <th>Produto</th>
            <th>Preço Unitário</th>
            <th>Quantidade</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
    @foreach($order->products as $product)
        <tr>
            <td>
                <p><strong><a href="{{route('products.show', [$product->slug])}}">{{ $product->name }}</a></strong></p>
            </td>
            <td>R$ {{ str_replace('.', ',', $product->price) }}</td>
            <td>{{ $product->pivot->quantity }}</td>
            <td>R$ {{ str_replace('.', ',', $product->getOriginalPriceFormat()*$product->pivot->quantity) }}</td>
       </tr>
    @endforeach
    </tbody>
</table>

<h3 class="pull-right">Valor total: R$ {{$order->total}}</h3>

</div>

</div>

<div class="row">
    <div class="col-xs-12">
        <h3>Dados de Entrega</h3>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>Endereço</th>
                    <td>{{$order->address->address}}</td>
                </tr>
                <tr>
                    <th>CEP</th>
                    <td>{{$order->address->zipcode}}</td>
                </tr>
                <tr>
                    <th>Estado</th>
                    <td>{{$order->address->state}}</td>
                </tr>
                <tr>
                    <th>Cidade</th>
                    <td>{{$order->address->city}}</td>
                </tr>
                <tr>
                    <th>Bairro</th>
                    <td>{{$order->address->region}}</td>
                </tr>
                <tr>
                    <th>Forma de Envio</th>
                    <td>{{$order->shipping->name}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h3>Dados de Pagamento</h3>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>Forma de pagamento</th>
                    <td>{{$order->payment->name}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h3>Pedidos já realizados</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Número do Pedido</th>
                    <th>Data</th>
                    <th>Produtos</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->user->orders as $order)
                <tr>
                    <th><a href="{{route('orders.show', [$order->number])}}">{{$order->number}}</a></th>
                    <td>{{$order->created_at}}</td>
                    <td>
                        @foreach($order->products as $product)
                            {{$product->name}} <strong>({{$product->pivot->quantity}})</strong> <br />
                        @endforeach
                    </td>
                    <td>
                        <strong>R$ {{$order->total}}</strong>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop