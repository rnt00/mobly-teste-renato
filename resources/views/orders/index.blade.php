@extends('app')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <h3>Finalizar pedido</h3>
        <hr/>
    </div>
</div>

   <h4>Dados de entrega</h4>

    @include('errors.list')

    {!! Form::open(['method' => 'POST', 'route'=>'orders.store', 'class'=>'form', 'role'=>'form']) !!}

        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    {!! Form::label('address', 'Endereço completo') !!}
                    {!! Form::text('address', null, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    {!! Form::label('state', 'Estado') !!}
                    {!! Form::text('state', null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {!! Form::label('city', 'Cidade:') !!}
                    {!! Form::text('city', null, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    {!! Form::label('region', 'Bairro') !!}
                    {!! Form::text('region', null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {!! Form::label('zipcode', 'CEP') !!}
                    {!! Form::text('zipcode', null, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div>

    <hr/>

    <h4>Dados de Envio</h4>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                {!! Form::label('shipping_id', 'Forma de envio') !!}
                {!! Form::select('shipping_id', [""=>"Selecione"]+$shippings, null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>

    <hr/>

    <h4>Dados de Pagamento</h4>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                {!! Form::label('payment_id', 'Forma de pagamento') !!}
                {!! Form::select('payment_id', [""=>"Selecione"]+$payments, null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary pull-right">
            Finalizar pedido
        </button>
    </div>

    {!! Form::close() !!}

@stop