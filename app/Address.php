<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address', 'state', 'city', 'region', 'zipcode'];

    /**
     * Get the order related with the given address
     */
    public function order(){
        return $this->hasOne('App\Order');
    }
}
