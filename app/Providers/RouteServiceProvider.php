<?php namespace App\Providers;

use App\Category;
use App\Order;
use App\Product;
use Auth;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
        $router->bind('category_slug', function($slug){
            return Category::where('slug', $slug)->firstOrFail();
        });

        $router->bind('product_slug', function($slug){
            return Product::where(['slug'=>$slug, 'status'=>1])->firstOrFail();
        });

        $router->bind('product_id', function($id){
            return Product::findOrFail($id);
        });

        $router->bind('order_number', function($number){
            return Auth::user()->orders()->where(['number'=>$number])->firstOrFail();
        });

		
		parent::boot($router);
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => $this->namespace], function($router)
		{
			require app_path('Http/routes.php');
		});
	}

}
