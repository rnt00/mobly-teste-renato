<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'lastname', 'email', 'password', 'birthdate', 'gender', 'cpf', 'status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    protected $dates = ['birthdate'];

    public  function orders(){
        return $this->hasMany('App\Order');
    }

    public function setBirthdateAttribute($date)
    {
        $this->attributes['birthdate'] = Carbon::parse($date);
    }

    public function getBirthdateAttribute($date)
    {
        return Carbon::parse($date)->format("Y-m-d");
    }

}