<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];


    /**
     * Get the products associated with the given category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products(){
        return $this->belongsToMany('App\Product')->withTimestamps();
    }

    /**
     * Convert category name into a slug
     *
     * @param $category_name
     */
    public function setSlugAttribute($category_name){
        $this->attributes['slug'] = Str::slug($category_name);
    }
}
