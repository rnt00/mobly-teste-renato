<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'image', 'price', 'weight', 'width', 'height', 'depth', 'status'];

    /**
     * Get the categories associated with the given product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category')->withTimestamps();
    }

    /**
     * Get the attributes associated with the given product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Attribute')->withPivot('value');
    }

    /**
     * Get orders associated with the given product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot('quantity');
    }

    /**
     * Convert product name into a slug
     *
     * @param $product_name
     */
    public function setSlugAttribute($product_name){
        $this->attributes['slug'] = Str::slug($product_name);
    }

    /**
     * Convert the price attribute separated by comma
     *
     * @param $price
     * @return string
     */
    public function getPriceAttribute($price){
        return number_format($price, 2, ',', '');
    }

    /**
     * Get the original database price attribute format
     *
     * @return mixed
     */
    public function getOriginalPriceFormat(){
        return $this->attributes['price'];
    }

    /**
     * Convert the width attribute separated by comma
     *
     * @param $width
     * @return string
     */
    public function getWidthAttribute($width){
        return number_format($width, 2, ',', '');
    }

    /**
     * Convert the height attribute separated by comma
     *
     * @param $height
     * @return string
     */
    public function getHeightAttribute($height){
        return number_format($height, 2, ',', '');
    }

    /**
     * Convert the depth attribute separated by comma
     *
     * @param $depth
     * @return string
     */
    public function getDepthAttribute($depth){
        return number_format($depth, 2, ',', '');
    }

    /**
     * Convert the weight attribute separated by comma
     *
     * @param $weight
     * @return string
     */
    public function getWeightAttribute($weight){
        return number_format($weight, 3, ',', '');
    }

    /**
     * Convert the image field to a full http url for the thumbnail
     *
     * @return string
     */
    public function getThumbImage(){
        return url('/uploads/products/thumbs').'/'.$this->attributes['image'];
    }

    /**
     * Convert the image field to a full http url for the full image
     *
     * @return string
     */
    public function getFullImage()
    {
        return url('/uploads/products') . '/' . $this->attributes['image'];
    }
}