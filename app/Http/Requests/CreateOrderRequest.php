<?php namespace App\Http\Requests;

use App\Http\Requests\Request;


class CreateOrderRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'address' => 'required|max:495',
            'state' => 'required|max:250',
            'city'  => 'required|max:250',
            'region' => 'required|max:250',
            'zipcode' => 'required|numeric|digits:8',
            'payment_id' => 'required|numeric',
            'shipping_id' => 'required|numeric'
		];
	}

    public function attributes(){
        return[
            'address' => 'Endereço completo',
            'state' => 'Estado',
            'city'  => 'Cidade',
            'region' => 'Bairro',
            'zipcode' => 'CEP',
            'payment_id' => 'Forma de pagamento',
            'shipping_id' => 'Forma de envio'
        ];
    }
}
