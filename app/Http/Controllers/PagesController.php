<?php namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\Config;

class PagesController extends Controller {

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $products = Product::paginate(Config::get('app.pagination_number'));
		return view('welcome', compact('products'));
	}

}
