<?php namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Payment;
use App\Shipping;
use Auth;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Order;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller {


    /**
     * Allow access only for authenticated users
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Create order form view
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(){
        if( !$this->verify_cart_content()){ return redirect('cart'); }
        $payments = Payment::lists('name', 'id');
        $shippings = Shipping::lists('name', 'id');

        return view('orders.index', compact('payments', 'shippings'));
    }

    /**
     * Store the order for the authenticated user
     *
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateOrderRequest $request){
        if( !$this->verify_cart_content()){ return redirect('cart'); }

        $address = Address::create($request->all());
        $order = Auth::user()->orders()->create([
            'payment_id' => $request->input('payment_id'),
            'shipping_id' => $request->input('shipping_id'),
            'address_id' => $address->id,
            'number' => strtoupper("MOBLY-".uniqid()),
            'total' => Cart::total()
        ]);

        $this->syncProducts($order);

        flash()->success("Pedido realizado com sucesso.");

        Cart::destroy();

        return redirect('order/show/'.$order->number);
    }

    public function show(Order $order){
        return view('orders.show', compact('order'));
    }

    /**
     * Verify if has products in the shopping cart
     *
     * @return bool
     */
    private function verify_cart_content(){
        if( Cart::count() < 1 ){
            flash()->error('É necessário ter produtos no carrinho para finalizar o pedido.');
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Associate the order with the shopping cart products
     *
     * @param Order $order
     */
    private function syncProducts(Order $order){
        $products = Cart::content();
        $prepared_data = [];
        foreach($products->toArray() as $product){
            $prepared_data[$product["id"]] = array("quantity" => $product["qty"]);
        }
        $order->products()->sync($prepared_data);
    }
}