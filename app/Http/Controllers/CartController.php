<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class CartController extends Controller {

    /**
     * Shows the cart view
     *
     * @return \Illuminate\View\View
     */
    public function index(){
        $cart = Cart::content();
        $total_qty = Cart::count();
        $total_price = Cart::total();
        return view('cart.index', compact('cart', 'total_qty', 'total_price'));
	}

    /**
     * Add items to the shopping cart
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Product $product)
    {
        $quantity = $request->input('quantity');

        if( !$this->validate_store_qty($quantity, $product) ){ return Redirect::back(); }

        Cart::associate('App\Product')->add($product->id, $product->name, $quantity, $product->getOriginalPriceFormat(), array('slug' => $product->slug));

        flash()->success("Produto <strong>".$product->name."</strong> adicionado ao carrinho com sucesso.");

        return redirect('cart');
    }

    /**
     * Remove all items of the shopping cart
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function clear(){
        Cart::destroy();
        flash()->success('Todos os produtos foram removidos.');
        return redirect('cart');
    }

    /**
     * Remove one row of the shopping cart
     *
     * @param $rowId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($rowId){
        $item = Cart::get($rowId);
        flash()->success('Produto <strong>'.$item->name.'</strong> removido do carrinho.');
        Cart::remove($rowId);
        return redirect('cart');
    }

    /**
     * Update amount of products in the cart
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request){

        $qty_list = $request->input('qty');
        $messages = '';
        if( !empty($qty_list) && is_array($qty_list) ){
            foreach($qty_list as $rowId => $qty){
                if( $qty < 1 ){
                    $qty = 1;
                    $messages .= "<strong>O número mínimo para cada produto é de 1 unidade</strong>";
                }
                if( $qty > 30 ){
                    $qty = 30;
                    $messages .= "<strong>O número máximo para cada produto é de 30 unidades</strong>";
                }
                Cart::update($rowId, $qty);
            }
        }

        flash()->success('Carrinho atualizado com sucesso.<br />'.$messages);

        return redirect('cart');
    }

    /**
     * Validate the min and max quantity of each product
     *
     * @param $quantity
     * @param Product $product
     * @return bool
     */
    private function validate_store_qty($quantity, Product $product){
        if(!empty($quantity) && $quantity > 0 && $quantity < 31){
            $cart = Cart::content();
            if(!$cart->isEmpty()){
                foreach($cart as $item){
                    if($item->id == $product->id && $quantity+$item->qty > 30){
                        flash()->error("É permitido somente 30 unidades de cada produto. Verifique seu carrinho.");
                        return FALSE;
                    }
                }
            }
        }else{
            flash()->error("Quantidade inválida: Digite um valor entre 1 e 30.");
            return FALSE;
        }

        return TRUE;
    }
}