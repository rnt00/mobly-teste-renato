<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Facades\Config;

use Illuminate\Http\Request;

class CategoriesController extends Controller {

    /**
     * List the products based on the given category
     *
     * @param Category $category
     * @return \Illuminate\View\View
     */
    public function index(Category $category)
    {
        $products = $category->products()->paginate(Config::get('app.pagination_number'));
        return view('categories.index', compact('category', 'products'));
	}
}