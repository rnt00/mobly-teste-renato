<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller {

    public function show(Product $product){
        return view('products.show', compact('product'));
    }

    public function search(Request $request){
        $keywords = explode(" ", trim(str_replace("\"", "", $request->get('q'))));

        if(count($keywords)==1 && empty($keywords[0])){
            return redirect("/");
        }else{
            $products = $this->searchProducts($keywords);
        }
        $keyword = $request->get('q');
        return view('products.search', compact('products', 'keyword'));
    }

    /**
     *
     *
     * @param $keyword
     * @return mixed
     */
    private function searchProducts($keywords){
        $where = $this->build_where_query($keywords);
        $products = Product::select('image', 'name', 'price', 'slug')
                        ->whereRaw($where)->paginate(Config::get('app.pagination_number'));
        return $products;
    }

    /**
     * Build custom query for multiple words
     *
     * @param $keywords
     * @return string
     */
    private function build_where_query($keywords){
        $fields = ['name','description','width', 'height','depth','slug'];
        $where = '';
        $clauses = [];
        foreach($fields as $field){
            foreach($keywords as $key => $keyword){
                $keyname = "keyword".$key;
                $keys[$keyname] = $keyword;
                $clauses[] = $field . ' LIKE "%'.$keyword.'%"';
            }
        }
        $where .= implode(' OR ', $clauses);
        return $where;
    }
}