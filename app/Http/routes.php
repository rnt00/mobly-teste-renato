<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// PagesController

Route::get('/', 'PagesController@index');

// CartController

Route::get('cart', array('as'=>'cart.index', 'uses'=>'CartController@index'));

Route::post('cart/store/{product_id}', array('as' => 'cart.store', 'uses' => 'CartController@store'));

Route::post('cart/update', array('as' => 'cart.update', 'uses' => 'CartController@update'));

Route::get('cart/remove/{row_id}', array('as' => 'cart.remove', 'uses' => 'CartController@destroy'));

Route::get('cart/clear', array('as' => 'cart.clear', 'uses' => 'CartController@clear'));

// OrdersController

Route::get('checkout', array('as'=>'orders.index', 'uses'=>'OrdersController@index'));
Route::post('order/store', array('as'=>'orders.store', 'uses'=>'OrdersController@store'));
Route::get('order/show/{order_number}', array('as'=>'orders.show', 'uses'=>'OrdersController@show'));

// ProductsController

Route::get('product/{product_slug}', array('as'=>'products.show', 'uses'=>'ProductsController@show'));

Route::get('/search', array('as'=>'products.search', 'uses'=>'ProductsController@search'));

// CategoriesController

Route::get('{category_slug}', array('as'=>'categories.index', 'uses' => 'CategoriesController@index'));

// AuthController

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);