<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'user_id', 'total', 'payment_id', 'address_id', 'shipping_id'];


    public function getTotalAttribute($total){
        return number_format($total, 2, ',', '');
    }

    /**
     * Get the products associated with the given order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity');
    }
    
    /**
     * Get the payment related with the order
     */
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }

    /**
     * Get the shipping method related to the order
     */
    public function shipping()
    {
        return $this->belongsTo('App\Shipping');
    }

    /**
     * Get the user that makes the order
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the delivery address for the order
     */
    public function address(){
        return $this->belongsTo('App\Address');
    }
}