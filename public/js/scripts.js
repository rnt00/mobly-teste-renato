function readyFn() {
    if ($('#confirm-delete').size() > 0) {
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    }
}

$(document).ready(readyFn());