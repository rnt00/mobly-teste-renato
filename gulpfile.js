var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('app.less');

    mix.styles([
        'googlefonts-roboto.css',
        'app.css'
    ], 'public/css/all.css', 'public/css');

    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
        'scripts.js'
    ], 'public/js/all.js', 'public/js');

    mix.version(['public/js/all.js', 'public/css/all.css']);
});
