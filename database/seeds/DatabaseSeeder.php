<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CategoriesTableSeeder');
	}

}

class CategoriesTableSeeder extends Seeder{
    public function run(){

        $categories = ['Móveis', 'Ambientes', 'Utilidades e Eletro', 'Decoração e Têxteis', 'Jardim e Lazer', 'Iluminação', 'Especiais', 'Tendências'];

    }
}
