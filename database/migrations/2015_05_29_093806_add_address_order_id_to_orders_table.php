<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressOrderIdToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
            $table->integer('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('address');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('orders', function($table)
        {
            $table->dropForeign('orders_address_id_foreign');
            $table->dropColumn('address_id');
        });
	}

}