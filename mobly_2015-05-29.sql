# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.19)
# Database: mobly
# Generation Time: 2015-05-29 19:16:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;

INSERT INTO `address` (`id`, `address`, `state`, `city`, `region`, `zipcode`, `created_at`, `updated_at`)
VALUES
	(5,'Av. Marquês de São Vicente, 2898 - Apto 45A','São Paulo','São Paulo','Água Branca','05036040','2015-05-29 12:14:56','2015-05-29 12:14:56'),
	(6,'Av. Marquês de São Vicente, 2898 - Apto 45A','São Paulo','São Paulo','Água Branca','05036040','2015-05-29 12:51:30','2015-05-29 12:51:30'),
	(7,'Av. Marquês de São Vicente, 2898 - Apto 45A','São Paulo','São Paulo','Água Branca','05036040','2015-05-29 12:53:21','2015-05-29 12:53:21'),
	(8,'Rua do Teste, 240','São Paulo','São Paulo','Penha','03502000','2015-05-29 15:55:22','2015-05-29 15:55:22'),
	(9,'Rua do Teste, 240','São Paulo','São Paulo','Penha','03502000','2015-05-29 15:58:51','2015-05-29 15:58:51'),
	(10,'Av. Marquês de São Vicente, 2898 - Apto 45A','São Paulo','São Paulo','Água Branca','05036040','2015-05-29 16:00:59','2015-05-29 16:00:59'),
	(11,'Rua do Teste, 240','São Paulo','São Paulo','Jardim Paulista','05036040','2015-05-29 16:02:02','2015-05-29 16:02:02');

/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attribute_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attribute_product`;

CREATE TABLE `attribute_product` (
  `attribute_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `attribute_product_attribute_id_foreign` (`attribute_id`),
  KEY `attribute_product_product_id_foreign` (`product_id`),
  CONSTRAINT `attribute_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_product_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `attribute_product` WRITE;
/*!40000 ALTER TABLE `attribute_product` DISABLE KEYS */;

INSERT INTO `attribute_product` (`attribute_id`, `product_id`, `value`)
VALUES
	(1,1,'Preto'),
	(2,1,'Sintético'),
	(3,1,'Sala de Estar'),
	(4,1,'6 Meses'),
	(5,1,'Usar na limpeza detergente de coco com pano humido , evitar exposição ao sol, evitar sentar suado na poltrona.'),
	(6,1,'1 Poltrona Multiuso'),
	(7,1,'Poltrona Multiuso Pelmex'),
	(8,1,'Não'),
	(1,2,'Vermelho'),
	(2,2,'Mantas de Algodão Natural com tecido Sarja de Algodão'),
	(3,2,'Sala de Estar'),
	(4,2,'12 Meses'),
	(5,2,'Não molhar-Limpeza escova de cerdas macias'),
	(6,2,'Manta Fardo / Plastico Bolha'),
	(7,2,'Futon'),
	(8,2,'Não'),
	(1,3,'Prata'),
	(2,3,'Aço'),
	(4,3,'6 Meses'),
	(6,3,'1 Panela de Pressão'),
	(7,3,'4930/101'),
	(8,3,'Não'),
	(1,4,'Preto'),
	(4,4,'3 Meses'),
	(5,4,'Improprio Para Micro Ondas'),
	(6,4,'Panela de Pressão 7,0 Litros - AA.'),
	(7,4,'557'),
	(2,4,'Alumínio'),
	(8,4,'Não');

/*!40000 ALTER TABLE `attribute_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;

INSERT INTO `attributes` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Cor','2015-05-28 01:18:32','2015-05-28 01:24:21'),
	(2,'Material','2015-05-28 01:18:45','2015-05-28 01:18:45'),
	(3,'Ambientes','2015-05-28 01:18:51','2015-05-28 01:18:51'),
	(4,'Garantia','2015-05-28 01:18:55','2015-05-28 01:18:55'),
	(5,'Cuidados','2015-05-28 01:19:05','2015-05-28 01:19:05'),
	(6,'Conteúdo da Embalagem','2015-05-28 01:19:13','2015-05-28 01:19:13'),
	(7,'Modelo','2015-05-28 01:19:23','2015-05-28 01:19:23'),
	(8,'Necessita Montagem?','2015-05-28 01:19:41','2015-05-28 01:19:41');

/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`)
VALUES
	(2,'Móveis','moveis','2015-05-27 18:00:45','2015-05-27 18:00:45'),
	(3,'Ambientes','ambientes','2015-05-27 18:01:27','2015-05-27 18:01:27'),
	(4,'Utilidades e Eletro','utilidades-e-eletro','2015-05-27 18:01:39','2015-05-27 18:01:39'),
	(5,'Decoração e Têxteis','decoracao-e-texteis','2015-05-27 18:01:53','2015-05-27 18:01:53'),
	(6,'Jardim e Lazer','jardim-e-lazer','2015-05-27 18:02:43','2015-05-27 18:02:43'),
	(7,'Iluminação','iluminacao','2015-05-27 18:02:53','2015-05-27 18:02:53'),
	(8,'Especiais','especiais','2015-05-27 18:03:02','2015-05-27 18:03:02'),
	(9,'Tendências','tendencias','2015-05-27 18:03:11','2015-05-27 18:03:11');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_product`;

CREATE TABLE `category_product` (
  `category_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `category_product_category_id_foreign` (`category_id`),
  KEY `category_product_product_id_foreign` (`product_id`),
  CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `category_product` WRITE;
/*!40000 ALTER TABLE `category_product` DISABLE KEYS */;

INSERT INTO `category_product` (`category_id`, `product_id`, `created_at`, `updated_at`)
VALUES
	(2,1,'2015-05-27 20:12:01','2015-05-27 20:12:01'),
	(3,1,'2015-05-27 20:12:01','2015-05-27 20:12:01'),
	(2,2,'2015-05-28 02:12:01','2015-05-28 02:12:01'),
	(8,2,'2015-05-28 02:12:01','2015-05-28 02:12:01'),
	(4,3,'2015-05-28 02:40:00','2015-05-28 02:40:00'),
	(9,3,'2015-05-28 02:40:00','2015-05-28 02:40:00'),
	(4,4,'2015-05-28 02:49:00','2015-05-28 02:49:00'),
	(5,4,'2015-05-28 02:49:00','2015-05-28 02:49:00'),
	(6,4,'2015-05-28 02:49:00','2015-05-28 02:49:00');

/*!40000 ALTER TABLE `category_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2015_05_27_144949_create_categories_table',1),
	('2015_05_27_145424_create_products_table',1),
	('2015_05_27_150806_create_attributes_table',1),
	('2015_05_27_151110_create_payments_table',1),
	('2015_05_27_151152_create_shippings_table',1),
	('2015_05_27_151316_create_orders_table',1),
	('2015_05_27_191711_alter_weight_size_from_products_table',2),
	('2015_05_29_092558_create_address_table',3),
	('2015_05_29_093806_add_address_order_id_to_orders_table',4),
	('2015_05_29_113610_add_quantity_to_product_order_table',5),
	('2015_05_29_114343_add_quantity_to_product_order_table',6),
	('2015_05_29_115954_alter_product_order_table_name',7);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table order_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_product`;

CREATE TABLE `order_product` (
  `product_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `quantity` smallint(5) unsigned NOT NULL,
  KEY `product_order_order_id_foreign` (`order_id`),
  KEY `product_order_product_id_foreign` (`product_id`),
  CONSTRAINT `product_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_order_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;

INSERT INTO `order_product` (`product_id`, `order_id`, `quantity`)
VALUES
	(1,1,1),
	(2,1,1),
	(1,2,10),
	(3,2,20),
	(4,3,5),
	(3,3,6),
	(2,4,30),
	(4,4,15),
	(3,4,10),
	(1,4,3),
	(2,5,1),
	(4,6,5);

/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `shipping_id` int(10) unsigned NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  KEY `orders_payment_id_foreign` (`payment_id`),
  KEY `orders_shipping_id_foreign` (`shipping_id`),
  KEY `orders_address_order_id_foreign` (`address_id`),
  CONSTRAINT `orders_address_order_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`),
  CONSTRAINT `orders_shipping_id_foreign` FOREIGN KEY (`shipping_id`) REFERENCES `shippings` (`id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `user_id`, `payment_id`, `shipping_id`, `total`, `number`, `created_at`, `updated_at`, `address_id`)
VALUES
	(1,1,2,1,969.98,'MOBLY-55688270B60CF','2015-05-29 12:14:56','2015-05-29 12:14:56',5),
	(2,1,4,3,13899.70,'MOBLY-55688B71A8884','2015-05-29 12:53:21','2015-05-29 12:53:21',7),
	(3,2,1,2,3659.89,'MOBLY-5568B61A6A080','2015-05-29 15:55:22','2015-05-29 15:55:22',8),
	(4,2,1,4,26009.42,'MOBLY-5568B6EBC8428','2015-05-29 15:58:51','2015-05-29 15:58:51',9),
	(5,2,1,3,599.99,'MOBLY-5568B76C0E0A6','2015-05-29 16:01:00','2015-05-29 16:01:00',10),
	(6,2,2,2,599.95,'MOBLY-5568B7AA7027D','2015-05-29 16:02:02','2015-05-29 16:02:02',11);

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;

INSERT INTO `payments` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Cartão de Crédito','2015-05-29 09:00:00','2015-05-29 09:00:00'),
	(2,'Débito Online','2015-05-29 09:00:00','2015-05-29 09:00:00'),
	(3,'Depósito','2015-05-29 09:00:00','2015-05-29 09:00:00'),
	(4,'Boleto','2015-05-29 09:00:00','2015-05-29 09:00:00');

/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `width` decimal(5,2) NOT NULL,
  `height` decimal(5,2) NOT NULL,
  `weight` decimal(5,3) NOT NULL,
  `depth` decimal(5,2) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `description`, `image`, `price`, `width`, `height`, `weight`, `depth`, `slug`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'Sofá-Cama Solteiro Multiuso Corino Preto','Fala sério, essa Poltrona Multiuso ficaria perfeita em sua sala de estar ou mesmo em seu escritório, né? Com design moderno e prático, ela cabe em pequenos espaços para que você possa aproveitar ao máximo seu ambiente de forma confortável. Fabricada em madeira de excelente qualidade e durabilidade, com preenchimento do assento em manta acrílica siliconada batida. Seu revestimento feito em corino na cor preta dá toda elegância ao móvel, deixando seu ambiente ainda mais bonito! Lindona, né?','sofa-cama-solteiro-multiuso-corino-preto.jpg',369.99,62.00,72.00,30.000,84.00,'sofa-cama-solteiro-multiuso-corino-preto',1,'2015-05-27 19:42:27','2015-05-27 19:42:27'),
	(2,'Sofá-Cama Solteiro Futon Listrado de Vermelho','O Sofá-Cama Tokyo foi feito especialmente para você que precisa otimizar o espaço de seu lar e não abre mão do conforto durante os momentos de descanso ou lazer. O móvel foi criado com estampa listrada e oferece uma decoração de muita personalidade e zen, mantendo o ambiente mais relaxante e tranquilo. O futon é preenchido com algodão e revestido em sarja, garantindo uma decoração sofisticada ao ambiente e muito aconchego a quem puder utilizá-lo. Incrível, não é mesmo?','sofa-cama-solteiro-futon-listrado-de-vermelho.jpg',599.99,90.00,30.00,11.800,80.00,'sofa-cama-solteiro-futon-listrado-de-vermelho',1,'2015-05-27 19:42:27','2015-05-27 19:42:27'),
	(3,'Panela De Pressão Inox 4,5L Supra Prata Brinox','Quer deixar a sua cozinha equipada com acessórios de qualidade para você preparar as mais deliciosas receitas? Então que tal adquirir a Panela de Pressão Supra Brinox? Com capacidade para até 3 litros, ela é ideal para você cozinhar aquele delicioso prato que será servido no domingo para toda a família! ;)','20150528KL71.jpg',509.99,44.20,51.50,1.700,25.50,'panela-de-pressao-inox-4-5l-supra-prata-brinox',1,'2015-05-28 02:38:00','2015-05-28 02:38:00'),
	(4,'Panela de Pressão 7,0 Litros - AA. 24x24','Quer deixar a sua cozinha equipada com acessórios de qualidade para você preparar as mais deliciosas receitas? Então que tal adquirir a Panela de Pressão AA? Com capacidade para até 7 litros, ela é ideal para você cozinhar aquela deliciosa feijoada que será servida no domingo para toda a família! ;)','20150528AB65.jpg',119.99,24.00,24.00,1.730,43.00,'panela-de-pressao-7-0-litros-aa-24x24',1,'2015-05-28 02:48:00','2015-05-28 02:48:00');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shippings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shippings`;

CREATE TABLE `shippings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `shippings` WRITE;
/*!40000 ALTER TABLE `shippings` DISABLE KEYS */;

INSERT INTO `shippings` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'SEDEX','2015-05-29 09:00:00','2015-05-29 09:00:00'),
	(2,'PAC','2015-05-29 09:00:00','2015-05-29 09:00:00'),
	(3,'Transportadora','2015-05-29 09:00:00','2015-05-29 09:00:00'),
	(4,'Motoboy','2015-05-29 09:00:00','2015-05-29 09:00:00');

/*!40000 ALTER TABLE `shippings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `cpf`, `birthdate`, `gender`, `status`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Renato','Peres','renato@teste.com.br','$2y$10$s8vCpryOwKLVaqJqiSJ.YelCS82xB04DFfYCJXESwjiPqZjQb3/X6','96385274128','1987-11-07','male',1,'f3OEFZzS2iVrphaTH768DPLa0txMyNbtYnoaLEuXIbtPiddzeXP5QZopmEDo','2015-05-28 23:22:59','2015-05-29 13:05:28'),
	(2,'José','Teste','teste@teste123.com.br','$2y$10$PYoU9TX78itNqlVOJmSCMeQ1r3qiT2v7AGH81qi6ln7O.LOtm3TAO','36510081857','1987-11-07','male',1,'0RWUjUTze3LhO4Km0rJOOD8sl8HjFFiY0sr0QC3TjMux4u8fC4lU3qxjn0D7','2015-05-29 15:54:29','2015-05-29 16:02:17');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
