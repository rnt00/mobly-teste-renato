Mobly Teste - Renato Peres
---------------------------

#### **Banco de Dados**
- Dump está na raiz: **mobly_2015-05-29.sql**

---

#### **Composer**
- Rodar "composer update"

---

#### **Arquivo de Configurações**
- Renomear o arquivo: **/.env.rename** para **/.env**
- Alterar as configuração conforme necessário

---

#### **Para rodar o projeto**
- php -S host:port -t public

---

#### **Recursos e Ferramentas Utilizadas**

- Framework Laravel 5
- Banco de dados MySQL
- PHP 5.5
- Key-value DB - Redis (Login e Carrinho de compras)
- Composer
- Bootstrap
- Gulp.js (Comprimir e Minificar)
- Testes Funcionais